from django.views.generic import ListView, DetailView
from product.models import Product


class ProductList(ListView):
    model = Product


class ProductDetail(DetailView):
    model = Product
