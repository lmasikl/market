from django.conf.urls import patterns, url
from product.views import ProductList, ProductDetail

urlpatterns = patterns('',
    url(r'^$', ProductList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', ProductDetail.as_view(), name='detail'),
)