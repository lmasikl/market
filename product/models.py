from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=128)
    # image = models.ImageField()
    # likes = models.PositiveIntegerField()

    def __unicode__(self):
        return u'{0}'.format(self.name)


class Offer(models.Model):
    VOLUME_CHOICES = (
        (0, '1 gr'),
        (1, '1 kg'),
    )
    product = models.ForeignKey(Product, related_name='offers')
    volume = models.SmallIntegerField(choices=VOLUME_CHOICES, default=1)
    price = models.DecimalField(max_digits=10, decimal_places=4)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.product.name, self.price)
