from django.core.urlresolvers import reverse
from django.test import TestCase
from account.models import User


class ViewTests(TestCase):
    def setUp(self):
        super(ViewTests, self).setUp()
        self.user = User.objects.create_user('user', 'user@momento.com', 'user')

    def test_products_list(self):
        response = self.client.get(reverse('order:product:list'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('product_list' in response.context)

    def test_product_detail(self):
        response = self.client.get(reverse('order:product:detail', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('product' in response.context)

    def test_take_offer(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('product:take-offer'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)
        response = self.client.post(reverse('product:take-offer'), data={'arg': 1})