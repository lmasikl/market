# coding=utf-8
from django.core.management.base import BaseCommand
from product.models import Product, Offer


class Command(BaseCommand):
    help = 'Create offer with product'
    args = 'product_name price'

    def handle(self, *args, **options):
        product, created = Product.objects.get_or_create(name=args[0])
        Offer.objects.create(product=product, price=args[1])