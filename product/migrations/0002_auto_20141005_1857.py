# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='volume',
            field=models.SmallIntegerField(default=1, choices=[(0, b'1 gr'), (1, b'1 kg')]),
        ),
    ]
