from django.core.urlresolvers import reverse
from django.test import TestCase
from account.models import User
from order.models import Order
from product.models import Product, Offer


class ViewTests(TestCase):

    def setUp(self):
        super(ViewTests, self).setUp()
        self.user = User.objects.create_user('user', 'user@momento.com', 'user')
        self.product = Product.objects.create(name='Product')
        self.offer = Offer.objects.create(product=self.product, price=1)
        self.order = Order.objects.create(offer=self.offer, user=self.user)

    def test_list(self):
        response = self.client.get(reverse('order:list'))
        self.assertEqual(response.status_code, 302)
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('order:list'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('order_list' in response.context)

    def test_create(self):
        response = self.client.get(reverse('order:create'))
        self.assertEqual(response.status_code, 302)
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('order:create'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)
        response = self.client.post(reverse('order:create'), data={'offer': 1})
        self.assertEqual(response.status_code, 302)

    def test_detail(self):
        response = self.client.get(reverse('order:detail', kwargs={'pk': self.order.pk}))
        self.assertEqual(response.status_code, 302)
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('order:detail', kwargs={'pk': self.order.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('order' in response.context)

    def test_update(self):
        response = self.client.get(reverse('order:update', kwargs={'pk': self.order.pk}))
        self.assertEqual(response.status_code, 302)
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('order:update', kwargs={'pk': self.order.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)
        self.assertTrue('order' in response.context)
        response = self.client.post(reverse('order:update', kwargs={'pk': self.order.pk}))
        self.assertEqual(response.status_code, 302)