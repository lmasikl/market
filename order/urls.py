from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from application import settings
from order.views import OrderList, OrderCreate, OrderDetail, OrderUpdate


urlpatterns = patterns('',
    url(r'^$', login_required(OrderList.as_view(), login_url=settings.LOGIN_URL), name='list'),
    url(r'^create/$', login_required(OrderCreate.as_view(), login_url=settings.LOGIN_URL), name='create'),
    url(r'^(?P<pk>\d+)/$', login_required(OrderDetail.as_view(), login_url=settings.LOGIN_URL), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', login_required(OrderUpdate.as_view(), login_url=settings.LOGIN_URL), name='update'),
)