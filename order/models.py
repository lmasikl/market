from django.core.urlresolvers import reverse
from django.db import models
from account.models import User
from product.models import Offer

class Address(models.Model):
    address = models.CharField(max_length=256)
    comment = models.TextField()


class Order(models.Model):
    STATUS_CHOICES = (
        (1, 'Created'),
        (2, 'Confirmed'),
        (3, 'Closed'),
    )
    offer = models.ForeignKey(Offer, related_name='orders')
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=1)
    user = models.ForeignKey(User, related_name='orders')
    # address = models.ForeignKey(Address, related_name='orders', blank=True, null=True)

    def __unicode__(self):
        return u'{0}: {1}'.format(self.pk, self.offer.product)

    def get_absolute_url(self):
        return reverse('order:detail', kwargs={'pk': self.pk})