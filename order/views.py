# coding=utf-8
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.views.generic import ListView, DetailView, CreateView, View, UpdateView
from order.models import Order
from product.models import Offer


class OrderView(View):
    model = Order

    def get_queryset(self):
        queryset = super(OrderView, self).get_queryset()
        user = self.request.user
        if user.is_superuser:
            return queryset

        return queryset.filter(user=self.request.user)


class OrderCreate(OrderView, CreateView):

    def post(self, request, *args, **kwargs):
        # FIXME: что то мне подсказывает, что это мощный костыль
        if 'pk' in request.POST:
            try:
                offer = Offer.objects.get(pk=request.POST['pk'])
            except Order.DoesNotExist:
                raise Http404

            order = Order.objects.create(offer=offer, user=request.user)
            return HttpResponseRedirect(reverse('order:detail', kwargs={'pk': order.pk}))

        return super(OrderCreate, self).post(request, *args, **kwargs)


class OrderDetail(OrderView, DetailView):
    pass


class OrderUpdate(OrderView, UpdateView):
    pass


class OrderList(OrderView, ListView):
    pass
