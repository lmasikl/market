from django.conf.urls import patterns, include, url
from product.views import ProductList

urlpatterns = patterns('',
    url(r'^$', ProductList.as_view(), name='frontpage'),

    url(r'^account/', include('account.urls', namespace='account')),

    url(r'^product/', include('product.urls', namespace='product')),

    url(r'^order/', include('order.urls', namespace='order')),
)
