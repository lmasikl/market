from django import forms
from account.models import User


class RegistrationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['confirm_password'] = forms.CharField()

    class Meta:
        model = User
        fields = ('username', 'password')