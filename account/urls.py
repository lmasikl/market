from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from account.views import Registration, UserDetail

urlpatterns = patterns('',
    url(r'^registration/', Registration.as_view(), name='registration'),
    url(r'^login/', 'django.contrib.auth.views.login', kwargs={'template_name': 'account/login.html'}, name='login'),
    url(r'^logout/', 'django.contrib.auth.views.logout', kwargs={'next_page': '/'}, name='logout'),
    url(r'^profile/', login_required(UserDetail.as_view()), name='detail'),
)
