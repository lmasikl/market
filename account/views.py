from django import forms
from django.forms.utils import ErrorList
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, FormView
from account.forms import RegistrationForm
from account.models import User


class Registration(FormView):
    form_class = RegistrationForm
    template_name = 'account/registration.html'

    def form_valid(self, form):
        data = form.cleaned_data
        password_confimed = data['password'] == data['confirm_password']
        if password_confimed:

            User.objects.create_user(username=data['username'], password=data['password'], email='not@e.mail')
            return HttpResponseRedirect(reverse('account:login'))

        # add form error
        errors = form._errors.setdefault(forms.forms.NON_FIELD_ERRORS, ErrorList())
        errors.append('Passwords not same')
        return self.render_to_response(self.get_context_data(form=form))

class UserDetail(DetailView):
    model = User

    def get_object(self, queryset=None):
        return self.request.user