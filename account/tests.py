from django.core.urlresolvers import reverse
from django.test import TestCase
from account.models import User


class ViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('user', 'user@momento.com', 'user')

    def test_registration(self):
        response = self.client.get(reverse('account:create'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)

    def test_login(self):
        response = self.client.get(reverse('account:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)

    def test_profile(self):
        self.client.login(user=self.user)
        response = self.client.get(reverse('account:detail'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('user' in response.context)